package br.com.neppo;

public class MathUtil {

	/**
	 * Dado um conjunto de números inteiros "ints" e um número arbitrário "sum",
	 * retorne true caso exista pelo menos um subconjunto de "ints" cuja soma soma
	 * dos seus elementos seja igual a "sum"
	 *
	 * @param ints
	 *            Conjunto de inteiros
	 * @param sum
	 *            Soma para o subconjunto
	 * @return
	 * @throws IllegalArgumentException
	 *             caso o argumento "ints" seja null
	 */
	public static boolean subsetSumChecker(int ints[], int sum) throws Exception {

		if (ints == null) {
			throw new IllegalArgumentException("Conjunto nao pode ser nulo!");
		}

		int a = 0;

		for (int i : ints) {

			a += i;

			if (a == sum) {

				return true;
			}

		}

		return false;

	}

}
